# Personal Tweets

Twitter Personal Manager for user Hashtags

## Getting Started


-  [Install and start RabbitMq ](   https://www.rabbitmq.com/download.html) 

-  [Install Tomcat 8.5 ](   https://tomcat.apache.org/download-80.cgi) 

- 	git clone https://gitlab.com/sharon.moshe/PersonalTweets.git

- 	cd PersonalTweets
 
- 	publish target/PersonalTweets.war to tomcat Web Server

- 	Start WebServer

- 	browse to localhost:8080/PersonalTweets



### UML

![Sequence Diagram](PeronalTweets_SequenceDiagram.png)


### Prerequisites

- RabbitMq

- Tomact 8.5




## Built With

* [RabbitMq ](   https://www.rabbitmq.com/download.html) - Queue Manager

* [Tomcat 8.5 ](   https://tomcat.apache.org/download-80.cgi) - Web Server

* [STS-Spring ](   https://spring.io/tools/sts/all) - Eclipse IDE

* [Maven](https://maven.apache.org/) - Dependency Management

* WebSockets Manager

* Twitter Stream Api



## Authors

* **Moshe Sharon** 



