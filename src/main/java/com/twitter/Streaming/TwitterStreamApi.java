package com.twitter.Streaming;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import com.twitter.db.SQLiteJDBC;
import com.twitter.rabbitmq.client.RabbitMqClient;
import twitter4j.HashtagEntity;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

public class TwitterStreamApi implements Runnable {

	static String hashTagQuery = null;																			//		query string with all the hashtags separated by 'or' 
	static final int count = 10;
	static long sinceId = 0;
	static long numberOfTweets = 0;
	static HashMap<String, List<String>> hashtagUsersMap;
	static final int MaxTweets = 180;
	
	
	@Override
	public void run() {
		  																// creates hashTagQuery  
		/* Using the inverted hashtagUsersMap we get all the user of specific hashtag*/
		setHashtagUserMap();																	
		ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setDebugEnabled(true).setOAuthConsumerKey("yt1l1aGzbugr2AVD3ZhEPCo0F")
				.setOAuthConsumerSecret("nDC32pU7UxNZ0nFwCzRYq02vxNTOEJ5AyaNWgI0f5P67moSrg1")
				.setOAuthAccessToken("19760885-PALPZnitFck4rey1V9zfuQkTk7dfx2sLb5d3fnC2n")
				.setOAuthAccessTokenSecret("OZhHHfaxCoMo65TlgXXD4h7DsEaLe0DIpIDhIjz6JjEVQ");
		TwitterFactory tf = new TwitterFactory(cb.build());
		Twitter twitter = tf.getInstance();

		// get latest tweets as of now
		// At this point store sinceId in database
		Query queryMax = new Query(hashTagQuery);
		queryMax.setCount(count);
		getTweets(queryMax, twitter);
		queryMax = null;

		// get tweets that may have occurred while processing above data
		// Fetch sinceId from database and get tweets, also at this point store the
		// sinceId
		
	
		do {
			Query querySince = new Query(hashTagQuery);
			querySince.setCount(count);
			querySince.setSinceId(sinceId);
			getTweets(querySince, twitter);
			querySince = null;
		} while (checkIfSinceTweetsAreAvaliable(twitter) && numberOfTweets < MaxTweets);
	}

	private static void setHashtagsQueryString() {
		String result = null;
		StringBuffer queryBuffer = new StringBuffer();
		SQLiteJDBC jdbc = new SQLiteJDBC();
		List<String> hashtags = jdbc.getHashtags();		
        hashtags.stream().forEach((hashtag)->{if (queryBuffer.indexOf(hashtag)<0) queryBuffer.append('#').append(hashtag).append(" OR ");});
		if (queryBuffer != null && queryBuffer.length() > 4) {
			result = queryBuffer.toString().substring(0, queryBuffer.length() - 4);
		}
		hashTagQuery =result;
	}

	public static  void setHashtagUserMap() {

		SQLiteJDBC jdbc = new SQLiteJDBC();
		hashtagUsersMap =  jdbc.getHashtagUserMap();
        setHashtagsQueryString();
	}

	private static boolean checkIfSinceTweetsAreAvaliable(Twitter twitter) {
		Query query = new Query(hashTagQuery);
		query.setCount(count);
		query.setSinceId(sinceId);
		try {
			QueryResult result = twitter.search(query);
			if (result.getTweets() == null || result.getTweets().isEmpty()) {
				query = null;
				return false;
			}
		} catch (TwitterException te) {
			System.out.println("(checkIfSinceTweetsAreAvaliable) Couldn't connect: " + te);
		} catch (Exception e) {
			System.out.println("(checkIfSinceTweetsAreAvaliable) Something went wrong: " + e);
		}
		return true;
	}

	private static void getTweets(Query query, Twitter twitter) {
		boolean getTweets = true;
		RabbitMqClient rabbitMqClient = new RabbitMqClient();
		while (getTweets  && numberOfTweets < MaxTweets) {
			try {		
				QueryResult result = twitter.search(query);
				Optional<List<Status>> statuses = Optional.of(result.getTweets());
				if (statuses.isPresent())
				{
					statuses.get().forEach((status->publishUserHashtags(rabbitMqClient, status)));
					numberOfTweets = numberOfTweets + statuses.get().size();
				}
			/*	if (result.getTweets() == null || result.getTweets().isEmpty()) {
					getTweets = false;
				} else {					

					for (Status status : result.getTweets())
					{
						publishUserHashtags(rabbitMqClient, status);
					}
					numberOfTweets = numberOfTweets + result.getTweets().size();
				}*/
			} catch (TwitterException te) {
				System.out.println("(getTweets) Couldn't connect: " + te);
				break;
			} catch (Exception e) {
				System.out.println("(getTweets) Something went wrong: " + e);
				break;
			}
		}
		System.out.println("Total tweets count=======" + numberOfTweets);
	}

	/**
	 * publishUserHashtags 
	 * - filter by requested hashtags
	 * - for every userId subscribed 
	 * 		- publish the status to userId(~queue)  
	 * @param rabbitMqClient
	 * @param status
	 */
	private static void publishUserHashtags(RabbitMqClient rabbitMqClient, Status status) {
		String hashtag;
		List<String> users = null;
		HashtagEntity[] entities = status.getHashtagEntities();						
		for (HashtagEntity entity : entities) {	
			hashtag = entity.getText().toLowerCase();
			
			if (hashTagQuery.indexOf(hashtag) > -1) {															
				users = hashtagUsersMap.get(hashtag);	
				if (users!=null)									
					for (String user : users) {
						rabbitMqClient.publish(user,hashtag +" : "+status.getText() );
					}
			}
		}
	}
	
	
	
}
