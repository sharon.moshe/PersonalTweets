package com.twitter;

import javax.servlet.ServletContextEvent;  
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.twitter.Streaming.TwitterStreamApi;
import com.twitter.db.SQLiteJDBC;
@WebListener
/**
 * 
 * @author moshe
 * When We start the web server 
 * - TwitterStreamApi is activated in a different thread 
 * - Subscribers table is created in DB if not exist already
 */
public class ContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
       
    	SQLiteJDBC jdbc = new SQLiteJDBC();
		jdbc.createSubscribersTable(); // create Subscribers table if doesn't exist
		Thread thread = new Thread(new TwitterStreamApi());
		thread.start();
    	
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        System.out.println("Shutting down!");
    }
}