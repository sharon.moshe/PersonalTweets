package com.twitter.rabbitmq.client;

/** 
 * @author moshe sharon
 */

/**
 * This class is used to
 * 				- creates queues with userId's names when Registering 
 * 				- publish the actual tweets which we get using TwitterStreamApi
 * 				- Receives the tweets when user ask for them in "Dispaly Tweets" screen
 * 
 */

import java.io.IOException;
import java.util.concurrent.TimeoutException;
import javax.websocket.Session;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.MessageProperties;
import com.rabbitmq.client.QueueingConsumer;

public class RabbitMqClient {
	ConnectionFactory factory = new ConnectionFactory();
	
	public RabbitMqClient() {
		factory.setUsername("guest");
		factory.setPassword("guest");
		factory.setVirtualHost("/");
		factory.setHost("127.0.0.1");
		factory.setPort(5672);
	}

	// creates queues with userId's names when Registering
	public void addUserQueue(String userId) throws IOException, TimeoutException {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("localhost");
		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();
		channel.queueDeclare(userId, true, false, false, null);
		channel.close();
		connection.close();

	}

	// publish the actual tweets which we get using TwitterStreamApi
	public void publish(String queueName, String mesage) {
		
		
		Connection conn;
		try {
			conn = factory.newConnection();
			Channel channel = conn.createChannel();
			byte[] messageBodyBytes = mesage.getBytes();
			channel.basicPublish("", queueName, MessageProperties.PERSISTENT_TEXT_PLAIN, messageBodyBytes);
			channel.close();
			conn.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// Receives the tweets when user ask for them in "Dispaly Tweets" screen
	public void consumer(String queueName, Session session) throws IOException {
		factory.setHost("localhost");
		Connection connection;
		connection = factory.newConnection();
		Channel channel = connection.createChannel();
		System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

		Consumer consumer = new QueueingConsumer(channel) {
			@Override
			public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
					byte[] body) throws IOException {
				String message = new String(body, "UTF-8");
				session.getBasicRemote().sendText(message);

			}
		};
		channel.basicConsume(queueName, true, consumer);
	}

}
