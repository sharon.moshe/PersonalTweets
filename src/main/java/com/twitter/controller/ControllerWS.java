package com.twitter.controller;

import java.io.IOException;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.server.ServerEndpoint;

import com.twitter.rabbitmq.client.RabbitMqClient;

import javax.websocket.Session;

/** 
 * @author moshe sharon
 */

/*
 * On RabbitMQ we built queues with userId's names 
 * when user enter is name into Display Tweets Screen and click on display tweets 
 * 		a message is sent and OnMessage function sends request for rabbitmq to get tweeets for this user 
 * 
 * 
 */


@ServerEndpoint("/websocket")
public class ControllerWS {
	
	 @OnOpen
	    public void onOpen(Session session){
	        System.out.println(session.getId() + " has opened a connection");
	    }
	 
	
	    @OnClose
	    public void onClose(Session session){
	        System.out.println("Session " +session.getId()+" has ended");
	    }
	
	// request tweets for the hashtags that the user entered when registered
    @OnMessage
    public void retrieveTweets(String userId, Session session){
        System.out.println("Get Tweets from " + session.getId() + ": for " + userId);
        RabbitMqClient rabbitMqClient = new RabbitMqClient();
        try {
        	rabbitMqClient.consumer(userId, session);              	
        } catch (IOException e) {
        	System.out.println("(consumer) Something went wrong: " + e.getMessage());					
        }
        catch (Exception e) {
        	System.out.println("(onMessage) Something went wrong: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     *  Method is called when an error occurs.
     *  
     * @param e
     */
	@OnError
	public void onError(Throwable e){
		e.printStackTrace();
	}



}
