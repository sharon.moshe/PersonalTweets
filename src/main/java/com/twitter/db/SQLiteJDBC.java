package com.twitter.db;





/** 
 * @author moshe sharon
 */


/**
 * In Order to have full persistence I choose to work with DB but for this project I choose a light one - SQLite
 * 	in Real Project MySql or NoSQL alternative will be chosen
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;


public class SQLiteJDBC {

	private Connection conn = null;
	private final String driver = "org.sqlite.JDBC";
	private final String connectionString = "jdbc:sqlite:test.db";
	private HashMap<String, List<String>> hashtagUserMap = new HashMap<String, List<String>>();

	public SQLiteJDBC() {

	}

	public Connection init() {
		conn = null;
		try {

			Class.forName(driver);
			conn = DriverManager.getConnection(connectionString);
			System.out.println("Opened database successfully");
			// setHashUserMap();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			//System.exit(0);
		}
		return conn;

	}
     
	/**
	 * getHashtagUserMap - create an inverted Hashmap between Hashtag and user for TwiterStreamAPI use
	 * @return HashtagUserMap
	 */
	public HashMap<String, List<String>> getHashtagUserMap() {
		conn = init();
		String userId = null;
		String messageId = null;
		HashMap<String, String> hashtags = new HashMap<String, String>();

		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM SUBSCRIBERS ;");
			while (rs.next()) {
				userId = rs.getString("USER_ID");
				messageId = rs.getString("FIRST_MESSAGE_ID");
				addToHashtagUserMap(messageId, userId);
				messageId = rs.getString("SECOND_MESSAGE_ID");
				addToHashtagUserMap(messageId, userId);
				messageId = rs.getString("THIRD_MESSAGE_ID");
				addToHashtagUserMap(messageId, userId);

			}
			rs.close();
			stmt.close();
			conn.close();

		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			//System.exit(0);
		}
		return hashtagUserMap;
	}
   /**
    * createSubscribersTable - create subscribers table if not exist already
    */
	public void createSubscribersTable() {
		Connection conn = init();
		Statement stmt = null;

		try {

			stmt = conn.createStatement();
			String sql = "CREATE TABLE IF NOT EXISTS SUBSCRIBERS " + "(USER_ID TEXT PRIMARY KEY     NOT NULL,"
					+ " FIRST_MESSAGE_ID           TEXT    , " + " SECOND_MESSAGE_ID          TEXT    , "
					+ " THIRD_MESSAGE_ID           TEXT    )";
			stmt.executeUpdate(sql);
			stmt.close();
			conn.close();
			System.out.println("Database Created successfully");

		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
		//	System.exit(0);
		}

	}
/*
 * insertHashTags - inserts new userid and hashtags 
 * !!!! for this project duplication error is ignored
 */
	public void insertHashTags(String userId, String[] hashtags) throws Exception {
		Connection conn = init();
		Statement stmt = null;
	
		try {
			conn.setAutoCommit(false);

			stmt = conn.createStatement();
			// TODO CHECK DUPLICATES
			StringBuffer filledHashtags = new StringBuffer();
			for (String hashtag : hashtags) {
				if (hashtag != null && !hashtag.isEmpty()) {
					if (hashtag.substring(0, 1).equals("#")) // if start with # remove it
					{
						hashtag = hashtag.substring(1);					
					}
					filledHashtags.append(",'").append(hashtag.toLowerCase()).append("' ");
				}
				else
				{
					filledHashtags.append(",NULL");

					
				}
			}
			String sql = "INSERT INTO SUBSCRIBERS (USER_ID,FIRST_MESSAGE_ID , SECOND_MESSAGE_ID , THIRD_MESSAGE_ID) "
					+ "VALUES ('" + userId + "'" + filledHashtags + " )";
			stmt.executeUpdate(sql);
			stmt.close();
			conn.commit();
			conn.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			throw e;
		}
		System.out.println("Records created successfully");

	}

	public List<String> getUserData(String userId) {
		String hashtag = null;
		Connection conn = init();
		Statement stmt = null;
		List<String> hashtags = new ArrayList<String>();
		try {
			conn.setAutoCommit(false);
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM SUBSCRIBERS WHERE USER_ID = '" + userId + "' ;");
			while (rs.next()) {
				hashtag = rs.getString("FIRST_MESSAGE_ID");
				if (hashtag != null)
					hashtags.add(hashtag);
				hashtag = rs.getString("SECOND_MESSAGE_ID");
				if (hashtag != null)
					hashtags.add(hashtag);
				hashtag = rs.getString("THIRD_MESSAGE_ID");
				if (hashtag != null)
					hashtags.add(hashtag);

			}
			rs.close();
			stmt.close();
			conn.close();

		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		return hashtags;

	}

	public void addToHashtagUserMap(String messageId, String userId) {
		if (messageId != null) {
			List<String> users = hashtagUserMap.get(messageId);
			if (users == null) {
				users = new LinkedList<String>();
			}
			users.add(userId);
			hashtagUserMap.put(messageId, users);

		}

	}

	// get list of hashtags
	public List<String> getHashtags() {

		String userId = null;
		String messageId = null;
		List<String> hashtags = new LinkedList<String>();
		try {
			conn = init();
			Statement statement = conn.createStatement();
			try {
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery("SELECT * FROM SUBSCRIBERS ;");
				while (rs.next()) {
					userId = rs.getString("USER_ID");
					messageId = rs.getString("FIRST_MESSAGE_ID");
					if (messageId != null && !messageId.equals(""))
						hashtags.add(messageId);
					messageId = rs.getString("SECOND_MESSAGE_ID");
					if (messageId != null && !messageId.equals(""))
						hashtags.add(messageId);
					messageId = rs.getString("THIRD_MESSAGE_ID");
					if (messageId != null && !messageId.equals(""))
						hashtags.add(messageId);

				}
				rs.close();
				stmt.close();
				conn.close();

			} catch (Exception e) {
				System.err.println(e.getClass().getName() + ": " + e.getMessage());
				System.exit(0);
			}

		} catch (SQLException e) {

			e.printStackTrace();
		}
		return hashtags;

	}

	

	private void dropTable() {
		try {
			Statement stmt = conn.createStatement();
			stmt.executeUpdate("DROP TABLE  SUBSCRIBERS;");
			stmt.close();
			conn.close();

		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}

	}

}