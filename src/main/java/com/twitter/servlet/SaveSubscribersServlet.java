package com.twitter.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeoutException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.twitter.Streaming.TwitterStreamApi;
import com.twitter.db.SQLiteJDBC;
import com.twitter.rabbitmq.client.RabbitMqClient;

@WebServlet(name = "UserServlet", urlPatterns = { "/save_user"})
public class SaveSubscribersServlet extends HttpServlet {

	HashMap<String, List<String>> hashtagUserMap = new HashMap<String, List<String>>();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		if (req.getRequestURI().indexOf("save_user") > -1) {
			System.out.println("Servlet " + this.getServletName() + " saving user");
			SQLiteJDBC jdbc = new SQLiteJDBC();
			String userId = req.getParameter("userId");

			String[] hashtags = req.getParameterValues("hash");
			try {
				jdbc.insertHashTags(userId, hashtags);
				RabbitMqClient client = new RabbitMqClient();
				// TODO handle under twitter stream manager
				try {
					TwitterStreamApi.setHashtagUserMap();
					client.addUserQueue(userId);
				} catch (TimeoutException e) {

					e.printStackTrace();
				}
				req.getRequestDispatcher("/index.html").forward(req, resp);
			} catch (Exception e) {
				if (e.getMessage().indexOf("CONSTRAINT") > -1) {
					String message = userId + "UserId  already exist";
					req.setAttribute("message", message);
					req.getRequestDispatcher("/register.jsp").forward(req, resp);
				}
			}


		} 

	}

	@Override
	public void init() throws ServletException {
		System.out.println("Servlet " + this.getServletName() + " has started");
		
	}

}