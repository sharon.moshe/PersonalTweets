
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<h2>Register</h2>
<style type="text/css">
.main {
	width: 700px;
	height: 200px;
	border: 2px solid blue;
	padding-top: 10px;
	padding-left: 10px;
	padding-right: 10px;
	padding-bottom: 10px;
}

.button {
	-webkit-transition-duration: 0.4s; /* Safari */
	transition-duration: 0.4s;
	box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0
		rgba(0, 0, 0, 0.19);
}

.button:hover {
	box-shadow: 0 12px 16px 0 rgba(0, 0, 0, 0.24), 0 17px 50px 0
		rgba(0, 0, 0, 0.19);
	background-color: #grey;
	color: blue;
}
</style>
<script>
	/*function validateForm() {
		if (document.frm.thirdHash.value != ""
				&& document.frm.secondHash.value == "") {
			alert("please fill the second hashtag first");
			document.frm.login.focus();
			return false;
		}
		if ((document.frm.firstHash.value.substring(0, 1) != '#')) {
			alert("please start with #");
			document.frm.login.focus();
			return false;
		}
	}*/

	form.addEventListener("submit", function(event) {
		if (document.frm.thirdHash.value != ""
				&& document.frm.secondHash.value == "") {
			alert("please fill the second hashtag first");
			document.frm.login.focus();
			return false;
		}
		if ((document.frm.firstHash.value.substring(0, 1) != '#')) {
			alert("please start with #");
			document.frm.login.focus();
			return false;
		}

	}, false);
</script>
</head>
<body>

	<form action="save_user" method="post" name="frm"
		onsubmit="return validateForm()">
		<div class="main">
			User Id : <input type="text" id="userId" name="userId"
				placeholder="User ID" required><br> <br />
			<br /> <label>Subscribe to the following hashtags</label> <br />
			<br /> <input type="text" name="hash" id="firstHash"
				placeHolder="first hashtag" required> <input type="text"
				name="hash" id="secondHash" placeHolder="second hashtag"> <input
				type="text" name="hash" id="thirdHash" placeHolder="third hashtag">
			<br />
			<br />
			<button type="submit" value="Submit" placeHolder="Submit"
				class="button">Save</button>

		</div>
	</form>

<c:if test="${not empty message}">
    <h1>${message}</h1>
</c:if>




</body>
</html>
